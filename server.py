from flask import Flask, request
from flask import render_template
from uuid import uuid4
import json
app=Flask(__name__)

#Historique
listPixel=[]
#Dict
dictPixel={}
#Création du tableau
tab = [] #Cette liste contiendra ma map en 2D
for i in range(30):
    tab.append([0] * 30) #Ajoute 30 colonnes de 30 entiers(int) ayant pour valeurs 0
class pixel:
    def __init__(self, x, y, color_index):
        self.x=x
        self.y=y
        self.color_index=color_index
    # def init(self, tab):
    #     self.tab=tab
    # def update(self, listPixel, tab)
    #     self.tab=tab
    #     tab[listPixel[-1].x][listPixel[-1].y]= listPixel[-1].color_index

@app.route('/place',methods=['POST'])
def place():
    body = request.json
    # tab=body['tab']
    # dictPixel['x']=body['x']
    # dictPixel['y']=body['y']
    # dictPixel['color_index']=body['color_index']
    listPixel.append(body)
    tab[body['x']][body['y']]=body['color']
    return ''

@app.route('/full',methods=['GET'])
def full():
    return json.dumps(tab)

@app.route('/histo',methods=['GET'])
def histo():
    return json.dumps(listPixel)


app.run(host='0.0.0.0')
