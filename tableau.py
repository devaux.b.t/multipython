from pygame import key, Rect, Surface, draw
from json import dumps
import requests 
import pygame
from cellule import cellule
# Définition de la classe Paddle

COLORS = [
    (255, 255, 255), # White
    (0, 0, 0), # Black
    (255, 0, 0), # Red
    (51, 204, 51), # Green
    (0, 153, 255), # Blue
    (255, 51, 204), # Pink
    (255, 153, 0), # Orange
    (255, 255, 0), # Yellow
    (153, 0, 204), # Purple
    (128, 128, 128), # Gray
    ]
class tableau():

    
    # Constructeur de la classe
    def __init__(self, screen):
        self.screen = screen
        self.monTableau = []
        return
        
    #tab = requests.get('http://127.0.0.1:5000/full')
    

    #création et initialisation du tableau
    def initTab(self):
        for i in range(30):
            self.monTableau.append([])
            for j in range(30):
                cell = cellule(i,j,(255,255,255))
                self.monTableau[i].append(cell.color_index)
        return

    #update tableau
    def updateTab(self, x, y, color):
        self.monTableau[x][y] = color

        body = { 'x': x, 'y': y, 'color': color}
        headers= { 'content-type': 'application/json' }

        requests.post(
            'http://92.94.243.152:5000/place',
            dumps(body),
            headers=headers
        )

        return

    def afficher(self):
        # Maintenant que les coordonées ont changés, on dessine le tableau
        for i in range(30):
            for j in range(30):
                index= self.monTableau[i][j]
                draw.rect(self.screen, COLORS[index], (i*30, j*30, 30, 30))
        #print(pygame.mouse.get_pos())
        return

    def recupTab(self):
        self.monTableau = requests.get('http://92.94.243.152:5000/full').json()
        return 

