from pygame import key, Rect, Surface, draw
# Définition de la classe Paddle
class cellule():

    # Constructeur de la classe
    def __init__(self, x, y, color_index):
        # position x du pixel
        self.x = x
        # position y du pixel
        self.y = y
        # La couleur du pixel
        self.color_index = color_index
        return

    # Méthode d'initialisation de l'objet, à exécuter une fois au début
    def init(self, screen: Surface):
        self.screen = screen
        return

    # Méthode de mise à jour de l'objet, à exécuter à chaque image
    def update(self, x, y, color_index):
        # position x du pixel
        self.x = x
        # position y du pixel
        self.y = y
        # La couleur du pixel
        if color_index == 1:
            self.color_index = (0,0,0)
        if color_index == 0:
            self.color_index = (255,255,255)

        return

    def afficher(self):
        # Maintenant que les coordonées ont changés, on dessine la raquette
        draw.rect(self.screen, self.color_index, (self.x, self.y, 30, 30))
        return