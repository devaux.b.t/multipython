import requests 
import  pygame
from pygame import display, key
import cellule
from tableau import tableau
from json import dumps
from pygame.constants import K_1, K_0, K_2, K_3, K_4, K_5, K_6, K_7, K_8, K_9

def main():
    # On initialise PyGame
    pygame.init()
    display.set_caption("MultiPython")

    # Crée une surface sur l'écran
    screen = display.set_mode((900, 900))

    # Indique si la boucle de jeu doit tourner ou non
    should_run = True
    color = 1
    
    #initialisation des objets
    monTableau = tableau(screen)
    monTableau.initTab()
    monTableau.recupTab()
    monTableau.afficher()

    compt = 0

    # Tant qu'on indique pas le contraire, on reste dans la boucle de jeu
    # Chaque tour de boucle représente une frame
    while should_run:
        

        # On met à jour l'affichage de l'écran pour afficher les modifs
        display.update()

        # On récupère tous les évènement de pygame
        for event in pygame.event.get():
            # On s'occupe uniquement de l'évènement QUIT (croix, alt+f4)
            if event.type == pygame.QUIT:
                # On met la variable running à False ce qui annule la boucle
                should_run = False
            elif key.get_pressed()[K_0]:
                color = 0
            elif key.get_pressed()[K_1]:
                color = 1
            elif key.get_pressed()[K_2]:
                color = 2
            elif key.get_pressed()[K_3]:
                color = 3
            elif key.get_pressed()[K_4]:
                color = 4
            elif key.get_pressed()[K_5]:
                color = 5
            elif key.get_pressed()[K_6]:
                color = 6
            elif key.get_pressed()[K_7]:
                color = 7
            elif key.get_pressed()[K_8]:
                color = 8
            elif key.get_pressed()[K_9]:
                color = 9
            

            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                x,y = pos
                monTableau.updateTab(x//30,y//30,color)
                monTableau.afficher()


        if compt >=5000:
            monTableau.recupTab()
            monTableau.afficher()
            compt = 0
        else:
            compt += 1

# On exécute la fonction main() uniquement si on lance le fichier main.py
# Si on importe le fichier main.py dans un autre fichier, __name__ ne
# sera pas égal à "__main__" et on n'exécutera pas le jeu
if __name__ == "__main__":
    main()

#-------------------------------------------------------------------------------------------------

# body = { 'id': 1, 'first_name': 'Bob', 'last_name': 'Watermelon', 'age': 12}

# # Headers HTTP pour indiquer le format au serveur
# headers= { 'content-type': 'application/json' }

# # Ici on effectue un POST avec notre body en JSON
#     requests.post(
#         'http://127.0.0.1:5000/hello/',
#         dumps(body),
#         headers=headers
#     )

# Ici on récupère un dictionnaire de valeurs*
#res = requests.post('http://localhost:5000/hello/').json()
# Le corps d'une requête au format dict aussi
#body = { 'x': 1, 'y': 1, 'color_index': 1}
# l_map[body['x']][body['y']]=[body['color_index'], 'tab': l_map]
# Headers HTTP pour indiquer le format au serveur
#headers= { 'content-type': 'application/json' }
# Ici on effectue un POST avec notre body en JSON
# requests.post(
#     'http://127.0.0.1:5000/place',
#     dumps(body),
#     headers=headers
#     )
# body2 = { 'x': 2, 'y': 2, 'color_index': 2}
# l_map[body['x']][body['y']]=[body['color_index'], 'tab': l_map]
# Headers HTTP pour indiquer le format au serveur
#headers= { 'content-type': 'application/json' }
# Ici on effectue un POST avec notre body en JSON
# requests.post(
#     'http://127.0.0.1:5000/place',
#     dumps(body2),
#     headers=headers
#     )
# #
